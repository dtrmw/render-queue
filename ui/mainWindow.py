from PyQt5 import QtGui, QtCore, QtWidgets

from models import rQueeItem as _rqItem
from . import mainWindowUI

class MainWindow(mainWindowUI.Ui_MainWindow):
	"""docstring for MainWindow"""
	def __init__(self):
		super(MainWindow, self).__init__()
		self._selectedRQItem = None
		self._isRendering = False

	def initializeUi(self, parent):
		self.setupUi(parent)
		#Connect Signals
		self.btnAdd.clicked.connect(self.addBlendFileToQuee)
		self.btnRemove.clicked.connect(self.removeBlendFileFromQuee)
		self.btnMoveUp.clicked.connect(self.moveUpSelected)
		self.btnMoveDown.clicked.connect(self.moveDownSelected)
		self.btnStartOrPause.clicked.connect(self.startOrPauseRenderQuee)
		self.btnCancel.clicked.connect(self.cancelRendering)
		self.btnBrowseOutputPath.clicked.connect(self.browseOutputPath)
		self.btnBrowseBlenderExecutable.clicked.connect(self.browseBlenderPath)
		self.listQuee.currentItemChanged.connect(self.listItemSelected)
		self.leFrames.editingFinished.connect(self.framesInputEdited)
		self.leScene.editingFinished.connect(self.sceneInputEdited)
		self.leNoOfThreads.editingFinished.connect(self.threadsInputEdited)
		self.leOutputPath.editingFinished.connect(self.outputPathInputEdited)

		self.process = QtCore.QProcess(parent)
		self.process.readyRead.connect(self.stdoutDataReady)
		self.process.started.connect(self.subProcessStarted)
		self.process.finished.connect(self.subProcessFinished)

	def addBlendFileToQuee(self):
		filePath = QtWidgets.QFileDialog.getOpenFileName(caption='Open Blend file', 
			filter='Blender Files (*.blend)')
		if filePath and filePath[0]:
			listItem = QtWidgets.QListWidgetItem()
			rqItem = _rqItem.RQueeItem(filePath[0])
			listItem.setData(QtCore.Qt.UserRole, rqItem)
			listItem.setText(filePath[0])
			self.listQuee.addItem(listItem)
			self.listQuee.setCurrentItem(listItem)

	def removeBlendFileFromQuee(self):
		currentIndex = self.listQuee.currentRow()
		currentItem = self.listQuee.takeItem(currentIndex)

	def moveUpSelected(self):
		currentIndex = self.listQuee.currentRow()
		currentItem = self.listQuee.takeItem(currentIndex)
		self.listQuee.insertItem(currentIndex-1, currentItem)
		self.listQuee.setCurrentItem(currentItem)

	def moveDownSelected(self):
		currentIndex = self.listQuee.currentRow()
		currentItem = self.listQuee.takeItem(currentIndex)
		self.listQuee.insertItem(currentIndex+1, currentItem)
		self.listQuee.setCurrentItem(currentItem)

	def startOrPauseRenderQuee(self):
		self._isRendering = True
		self.renderTopItem()


	def cancelRendering(self):
		self._isRendering = False
		self.process.terminate()

	def browseOutputPath(self):
		outputPath = QtWidgets.QFileDialog.getSaveFileName(None, 
			'Output Path & File Name')
		self.leOutputPath.setText(outputPath[0] if outputPath else "")
		self.outputPathInputEdited()

	def browseBlenderPath(self):
		outputPath = QtWidgets.QFileDialog.getOpenFileName(caption='Select Blender Executable')
		self.leBlenderExecutable.setText(outputPath[0] if outputPath else "")

	def listItemSelected(self, item):
		if not item:
			print ("No item selected")
			return

		# self._selectedRQItem = item.data(QtCore.Qt.UserRole).toPyObject()
		self._selectedRQItem = item.data(QtCore.Qt.UserRole)
		self.gbOptions.setEnabled(not self._selectedRQItem.isRendering())
		self.leFrames.setText(self._selectedRQItem.frames)
		self.leScene.setText(self._selectedRQItem.scene or "")
		self.leNoOfThreads.setText(self._selectedRQItem.threads or "")
		self.leOutputPath.setText(self._selectedRQItem.outputPath or "")

	def framesInputEdited(self):
		self._selectedRQItem.frames = self.leFrames.text()
	
	def sceneInputEdited(self):
		self._selectedRQItem.scene = self.leScene.text()

	def threadsInputEdited(self):
		self._selectedRQItem.threads = self.leNoOfThreads.text()
		
	def outputPathInputEdited(self):
		self._selectedRQItem.outputPath = self.leOutputPath.text()

	def subProcessStarted(self):
		self.btnStartOrPause.setEnabled(False)

	def subProcessFinished(self):
		self.btnStartOrPause.setEnabled(True)
		if self._isRendering:
			self.listQuee.takeItem(0)
			self.renderTopItem()

	def renderTopItem(self):
		blenderExecutable = self.leBlenderExecutable.text() or "C:\Program Files\Blender Foundation\Blender"
		topItemInQuee = self.listQuee.itemAt(0,0)
		if not topItemInQuee:
			print ("No items are there to render")
			return
		# rqItemData = topItemInQuee.data(QtCore.Qt.UserRole).toPyObject()
		rqItemData = topItemInQuee.data(QtCore.Qt.UserRole)
		options = rqItemData.getOptions()
		rqItemData.setRendering(True)
		self.process.start(blenderExecutable, options)

		print (blenderExecutable)
		print (options)

	def stdoutDataReady(self):
		newText = str(self.process.readAll())
		if newText.startswith("Saved"):
			self.teConsoleOutput.setText("")
		cursor = self.teConsoleOutput.textCursor()
		cursor.movePosition(cursor.End)
		cursor.insertText(newText)
		self.teConsoleOutput.ensureCursorVisible()
		self.statusbar.showMessage(newText)

