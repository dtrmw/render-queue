

class RQueeItem(object):
	"""docstring for RQueeItem"""
	def __init__(self, filePath):
		super(RQueeItem, self).__init__()
		self._filePath = filePath
		self._frames = "anim"
		self._outputPath = None
		self._threads = None
		self._scene = None
		self._isRendering = False

	@property
	def filePath(self):
		return self._filePath

	@filePath.setter
	def filePath(self, value):
		self._filePath = value

	@property
	def frames(self):
		return self._frames

	@frames.setter
	def frames(self, value):
		self._frames = value

	@property
	def outputPath(self):
		return self._outputPath

	@outputPath.setter
	def outputPath(self, value):
		self._outputPath = value

	@property
	def threads(self):
		return self._threads

	@threads.setter
	def threads(self, value):
		self._threads = value

	@property
	def scene(self):
		return self._scene

	@scene.setter
	def scene(self, value):
		self._scene = value

	def isRendering(self):
		return self._isRendering

	def setRendering(self, value):
		self._isRendering = value

	def getOptions(self):
		options = ["-b", str(self._filePath)]
		if self._scene:
			options.append("--scene")
			options.append(str(self._scene))
		
		if self._outputPath:
			options.append("--render-output")
			options.append(str(self._outputPath))

		if self._threads and int(self._threads)> 0:
			options.append("--threads")
			options.append(str(self._threads))

		if self._frames == "anim":
			options.append("--render-anim")
		else:
			fInput = str(self._frames).split(" ")
			if len(fInput) == 1:
				options.append("--render-frame")
				options.append(fInput[0])
			else:
				fOpts = ["--frame-start", "--frame-end", "--frame-jump"]
				for i in range(len(fInput)) :
					options.append(fOpts[i])
					options.append(fInput[i])
				options.append("--render-anim")
		return options
	
	
	
	
		