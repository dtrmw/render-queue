from PyQt5 import QtGui, QtWidgets, QtCore
from ui import mainWindow

def main():
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = mainWindow.MainWindow()
    ui.initializeUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()